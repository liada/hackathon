class JoinpostsController < ApplicationController
  before_action :logged_in_user

  def create
    activity = Micropost.find(params[:post_id])
    current_user.join(activity)
    redirect_to root_url 
  end

  def destroy
    activity = Joinpost.where(post_id: params[:id]).first.post
    current_user.unjoin(activity)
    redirect_to root_url
  end
end
