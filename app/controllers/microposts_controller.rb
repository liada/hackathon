class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    @micropost = current_user.microposts.build(micropost_params)
    @micropost.users_attending = 1 #temporary 
    d = Date.parse(@micropost.date)
    t = Time.parse(@micropost.hours + ":" + @micropost.minutes)
    zone = Time.now.in_time_zone('Pacific Time (US & Canada)').zone
    @micropost.meeting_time = DateTime.new(d.year, d.month, d.day, t.hour, t.min, t.sec, zone)
    if @micropost.save
      current_user.join(@micropost)
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def show 
    @micropost = Micropost.find(params[:id])
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to request.referrer || root_url
  end

  private

    def micropost_params
      params.require(:micropost).permit(:restaurant_name, :meeting_time, :max_spots,
                                        :meeting_location, :content, :picture, 
                                        :hours, :minutes, :date)
    end
    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
