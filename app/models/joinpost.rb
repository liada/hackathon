class Joinpost < ApplicationRecord
  belongs_to :joiner, class_name: "User"
  belongs_to :post, class_name: "Micropost"
  validates :joiner_id, presence: true
  validates :post_id, presence: true
end
