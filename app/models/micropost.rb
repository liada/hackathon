class Micropost < ApplicationRecord
  attr_accessor :minutes, :hours, :date
  belongs_to :user
  has_many :joiner_relationships, class_name: "Joinpost",
                                  foreign_key: :post_id,
                                  dependent: :destroy
  has_many :joiners, through: :joiner_relationships
  default_scope -> { order(meeting_time: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, length: { maximum: 140 }
  validate  :picture_size
  validates :restaurant_name, presence: true
  validates :meeting_location, presence: true
  #validates :users_attending, presence: true
  validates :meeting_time, presence: true
  validate  :meeting_time_not_past
  validates :date, presence: true
  validates :minutes, presence: true
  validates :hours, presence: true

  private

    def meeting_time_not_past
      if meeting_time < Time.now
        errors.add(:meeting_time, "should not be in the past")
      end
    end

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
