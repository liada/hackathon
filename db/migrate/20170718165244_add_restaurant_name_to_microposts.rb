class AddRestaurantNameToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :restaurant_name, :string
  end
end
