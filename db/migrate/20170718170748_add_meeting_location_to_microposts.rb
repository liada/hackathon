class AddMeetingLocationToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :meeting_location, :string
  end
end
