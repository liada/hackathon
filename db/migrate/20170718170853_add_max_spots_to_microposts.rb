class AddMaxSpotsToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :max_spots, :integer
  end
end
