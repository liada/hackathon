class AddMeetingTimeToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :meeting_time, :datetime
  end
end
