class AddUsersAttendingToMicroposts < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :users_attending, :int, array: true, default: []
  end
end
