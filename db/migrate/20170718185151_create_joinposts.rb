class CreateJoinposts < ActiveRecord::Migration[5.0]
  def change
    create_table :joinposts do |t|
      t.integer :joiner_id
      t.integer :post_id

      t.timestamps
    end
  end
end
